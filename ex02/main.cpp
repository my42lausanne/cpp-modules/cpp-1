/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 11:21:23 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 10:20:43 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

using std::string;
using std::cout;
using std::endl;

int	main( void )
{
	string	str = "HI THIS IS BRAIN";
	string	*stringPTR = &str;
	string	&stringREF = str;

	cout << &str << endl;
	cout << stringPTR << endl;
	cout << &stringREF << endl;

	cout << endl;

	cout << str << endl;
	cout << *stringPTR << endl;
	cout << stringREF << endl;
}
