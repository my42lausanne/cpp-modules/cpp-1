/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 10:44:36 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 11:15:26 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

Zombie	*zombieHorde( int N, std::string name)
{
	Zombie	*z;

	z = new Zombie[N];
	for (int i = 0; i < N; i++)
		z[i].set_name(name);
	return (z);
}
