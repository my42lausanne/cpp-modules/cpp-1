/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 10:44:19 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 11:15:39 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

Zombie	*zombieHorde( int N, std::string name);

int	main( void )
{
	const int	N = 5;
	Zombie		*z = zombieHorde(N, "Zoooombie");

	for (int i = 0; i < N; i++)
		z[i].announce();
	delete [] z;
	return (0);
}
