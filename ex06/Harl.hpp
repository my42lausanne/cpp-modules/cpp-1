/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:40:46 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 12:07:58 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HARL_HPP
# define HARL_HPP

#include <iostream>

using std::cout;
using std::endl;
using std::string;

class Harl
{
	typedef struct s_levels
	{
		string	lname;
	}	t_levels;

	private:
		void	debug( void );
		void	info( void );
		void	warning( void );
		void	error( void );
		void	other( void );

		t_levels	levels[4];
		int			filter;
		int			was_filtered;

	public:
		Harl( string filter );

		void	complain( string level );
};

#endif
