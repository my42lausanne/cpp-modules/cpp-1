/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:47:48 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 12:07:48 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"

void	str_tolower( string &str )
{
	for (unsigned long i = 0; i < str.length(); i++)
		str[i] = tolower(str[i]);
}

void	str_toupper( string &str )
{
	for (unsigned long i = 0; i < str.length(); i++)
		str[i] = toupper(str[i]);
}

Harl::Harl( string filter )
{
	was_filtered = 0;

	levels[0].lname = "debug";
	levels[1].lname = "info";
	levels[2].lname = "warning";
	levels[3].lname = "error";

	this->filter = 5;
	str_tolower(filter);
	for (int i = 0; i < 4; i++)
	{
		if (filter == levels[i].lname)
			this->filter = i;
	}
}

void	Harl::complain( string level )
{
	int	complaint = -1;

	for (int i = 0; i < 4; i++)
	{
		if (level == levels[i].lname && i >= filter)
			complaint = i;
	}

	if (complaint >= 0)
	{
		if (was_filtered)
			cout << endl;
		was_filtered = 0;
		str_toupper(level);
		cout << "[ " << level << " ]\n";
	}

	switch (complaint)
	{
		case 0:
			Harl::debug();
			break;
		case 1:
			Harl::info();
			break;
		case 2:
			Harl::warning();
			break;
		case 3:
			Harl::error();
			break;
		default:
			if (!was_filtered)
			{
				was_filtered = 1;
				Harl::other();
			}
			was_filtered = 2;
	}
	if (complaint >= 0)
		cout << endl;
}

void	Harl::debug( void )
{
	cout << "I love having extra bacon for my 7XL-double-cheese-triple-pickle-special-ketchup burger. I really do!\n";
}

void	Harl::info( void )
{
	cout << "I cannot believe adding extra bacon costs more money. You didn’t put enough bacon in my burger! If you did, I wouldn’t be asking for more!\n";
}

void	Harl::warning( void )
{
	cout << "I think I deserve to have some extra bacon for free. I’ve been coming for years whereas you started working here since last month.\n";
}

void	Harl::error( void )
{
	cout << "This is unacceptable! I want to speak to the manager now.\n";
}

void	Harl::other( void )
{
	cout << "[ Probably complaining about insignificant problems ]\n";
}
