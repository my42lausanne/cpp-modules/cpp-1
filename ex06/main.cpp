/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:40:13 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 11:53:20 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"
#include <iostream>

int	main( int ac, char **av )
{
	if (ac != 2)
	{
		std::cerr << "Wrong number of arguments\n";
		return (1);
	}
	Harl	h(av[1]);

	h.complain("debug");
	h.complain("info");
	h.complain("warning");
	h.complain("error");
	return (0);
}
