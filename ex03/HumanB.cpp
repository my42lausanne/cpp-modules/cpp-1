/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:08:01 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 12:44:10 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

using std::cout;
using std::endl;

HumanB::HumanB( string name )
{
	this->w = NULL;
	this->name = name;
}

void	HumanB::setWeapon( Weapon &w )
{
	this->w = &w;
}

void	HumanB::attack( void )
{
	if (w)
		cout << name  << " attacks with their " << w->getType() << endl;
}
