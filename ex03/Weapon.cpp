/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 11:30:06 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 11:58:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon( void ) { }

Weapon::Weapon( string type )
{
	this->type = type;
}

const string	&Weapon::getType( void )
{
	return (type);
}

void	Weapon::setType( string type )
{
	this->type = type;
}
