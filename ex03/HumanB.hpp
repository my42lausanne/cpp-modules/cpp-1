/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:05:48 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 12:43:25 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP

#include "Weapon.hpp"

using std::string;

class HumanB
{
	private:
		string	name;
		Weapon	*w;

	public:
		HumanB( string name );

		void	setWeapon( Weapon &w );
		void	attack( void );
};

#endif
