/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 11:38:04 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 12:00:36 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP

#include "Weapon.hpp"

using std::string;

class HumanA
{
	private:
		string	name;
		Weapon	&w;

	public:
		HumanA( string name, Weapon &w );

		void	attack( void );
};

#endif
