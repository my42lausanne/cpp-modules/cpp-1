/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 11:30:10 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 11:49:59 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
# define WEAPON_HPP

#include <iostream>

using std::string;

class Weapon
{
	private:
		string	type;

	public:
		Weapon( void );
		Weapon( string type );
		const string	&getType( void );
		void			setType( string type );
};

#endif
