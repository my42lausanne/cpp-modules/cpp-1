/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 11:38:46 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 12:04:36 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

using std::cout;
using std::endl;

HumanA::HumanA( string name, Weapon &w ) : w(w)
{
	this->name = name;
}

void	HumanA::attack( void )
{
	cout << name  << " attacks with their " << w.getType() << endl;
}
