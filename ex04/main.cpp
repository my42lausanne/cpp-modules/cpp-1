/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:51:02 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 10:58:55 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sed.hpp"
#include <sstream>

int	main( int ac, char **av )
{
	if (ac != 4)
	{
		cerr << "Wrong number of arguments\n";
		return (1);
	}

	fstream	infile, outfile;
	if (read_file(av[1], infile) || write_file(av[1], outfile))
		return (1);
	string	content = string(
		(std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
	outfile << replace(content, av[2], av[3]);
	infile.close();
	outfile.close();
	return (0);
}
