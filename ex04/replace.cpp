/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 13:26:37 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 11:33:05 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sed.hpp"
#include <cstring>

string	replace(string str, const char *s1, const char *s2)
{
	size_t	pos;
	size_t	last_pos = 0;
	while (1)
	{
		if ((pos = str.find(s1, last_pos)) == string::npos)
			break ;
		last_pos = pos + std::strlen(s1);
		str = str.substr(0, pos ) + s2 + str.substr(pos + std::strlen(s1));
		last_pos = pos + std::strlen(s2);
	}
	return (str);
}
