/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   open_files.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:57:27 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/28 13:31:46 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sed.hpp"

int	read_file( const string filename, fstream &file)
{
	file.open(filename.c_str(), ios::in);
	if (!file.is_open())
	{
		cerr << "There was an error opening " << filename << endl;
		return (1);
	}
	return (0);
}

int	write_file( string filename, fstream &file)
{
	filename += ".replace";
	file.open(filename.c_str(), ios::out | ios::trunc);
	if (!file.is_open())
	{
		cerr << "There was an error opening " << filename << endl;
		return (1);
	}
	return (0);
}
