#!/bin/sh

EXEC_NAME="replace"

for file in test_files/*.test ; do
	./../$EXEC_NAME $file string1 str2
	cp $file $file.sed
	sed -i '' 's/string1/str2/g' $file.sed
	DIFF=$(diff $file.replace $file.sed)
	if [ ! -z "$DIFF" ] ; then
		echo $file
		echo $DIFF
	fi
done
