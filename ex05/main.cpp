/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:40:13 by dfarhi            #+#    #+#             */
/*   Updated: 2022/10/20 11:38:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"
#include <iostream>

int	main( void )
{
	Harl	h;

	h.complain("info");
	h.complain("debug");
	h.complain("warning");
	h.complain("error");
	h.complain("inaudible");
	return (0);
}
