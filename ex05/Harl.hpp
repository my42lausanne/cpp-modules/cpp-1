/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:40:46 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/29 22:52:57 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HARL_HPP
# define HARL_HPP

#include <iostream>

using std::cout;
using std::endl;
using std::string;

class Harl
{
	typedef struct s_levels
	{
		string	lname;
		void	(Harl::*f)( void );
	}	t_levels;

	private:
		void	debug( void );
		void	info( void );
		void	warning( void );
		void	error( void );

		t_levels	levels[4];

	public:
		Harl( void );

		void	complain( string level );
};

#endif
