/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/29 22:47:48 by dfarhi            #+#    #+#             */
/*   Updated: 2022/06/29 23:17:59 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Harl.hpp"

Harl::Harl( void )
{
	levels[0].lname = "debug";
	levels[0].f = &Harl::debug;
	levels[1].lname = "info";
	levels[1].f = &Harl::info;
	levels[2].lname = "warning";
	levels[2].f = &Harl::warning;
	levels[3].lname = "error";
	levels[3].f = &Harl::error;
}

void	Harl::complain( string level )
{
	for (int i = 0; i < 4; i++)
	{
		if (level == levels[i].lname)
			(this->*(levels[i].f))();
	}
}

void	Harl::debug( void )
{
	cout << "I love having extra bacon for my 7XL-double-cheese-triple-pickle-special-ketchup burger. I really do!\n";
}

void	Harl::info( void )
{
	cout << "I cannot believe adding extra bacon costs more money. You didn’t put enough bacon in my burger! If you did, I wouldn’t be asking for more!\n";
}

void	Harl::warning( void )
{
	cout << "I think I deserve to have some extra bacon for free. I’ve been coming for years whereas you started working here since last month.\n";
}

void	Harl::error( void )
{
	cout << "This is unacceptable! I want to speak to the manager now.\n";
}
